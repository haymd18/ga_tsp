// Raja Sooriamurthi
// S528 Object Oriented Programming

import java.util.Arrays;

public class TSP { 

	final City[] cities;
	final int MAX_GEN = 5000;
	final int MAX_CITIES_ON_SIDE;
	static double p_mutate = 0.1;
	final int POPL_SIZE = 1000;
	final double EPSILON = 0.1;
	
	Map map;
	Tour[] new_pool, old_pool;

	

	TSP(int command) {
		MAX_CITIES_ON_SIDE = command ;
		
		this.cities = GenerateCities.square(MAX_CITIES_ON_SIDE);
		Tour.tsp = this;
		this.map = new Map(this);

		old_pool = new Tour[POPL_SIZE];
		new_pool = new Tour[POPL_SIZE];
	}

	int numCities() {
		return cities.length;
	}

	void run() {

		// create an initial random population of tours

		for (int i = 0; i < POPL_SIZE; i++) {
			old_pool[i] = Tour.rndTour();
		}

		Tour[] tmp_pool;

		// show best initial random tour
		Arrays.sort(old_pool);
		old_pool[0].display(map);
		Util.pause(2000);

		// run one sweep of the GA 100 times
		for (int k = 0; k < MAX_GEN; k++) {
			if (k % 10 == 0) { // change the 1 to what ever interval you want
				showPool(old_pool, String.format("Gen: %3d", k), 5);
				map.gen = k;
				// display the lowest distance (i.e. fittest) tour
				old_pool[0].display(map);
				Util.pause(250);
			
				
			//changing the mutation coefficent to %50 written by Brendan Scott
			if(k%20 == 0) {
				p_mutate = p_mutate*1.5;
			}
			}//if k
			
			//Matt Hay -Elitist
			
			//add 10 unchanged best parents to new child pool (they are already ordered in old_pool)
			for(int i = 0; i<10; i++)
				new_pool[i] = old_pool[i];
			
			// reset the index, index starts where best parents left off; -- see below
			new_i = 10;

			for (int i = 0; i < POPL_SIZE / 2 - 5; i++) {
				do_ga(); // new children are added to new_pool
			}
			// interchange the new and old pools
			tmp_pool = old_pool;
			old_pool = new_pool;
			new_pool = tmp_pool;
			// sort the old_pool in ascending order
			Arrays.sort(old_pool);
			
			// Epsilon stopping condition made by Tyler Keyes
			
			// calculates the difference between the distance of the best tour from the old
			// pool and the best tour from the new pool, then compares that to a set EPSILON
			// value to determine if the fitness doesn't improve
			double oldPool = 0.0;
			double newPool = 0.0;
			for(int i=0; i<POPL_SIZE; i++) {
				oldPool += old_pool[i].totalDistance();
				newPool += new_pool[i].totalDistance();
			}
			
			oldPool = oldPool / POPL_SIZE;
			newPool = newPool / POPL_SIZE;
			 
			double difference = Math.abs(oldPool - newPool);
			
			if (difference <= EPSILON) {
				break; //we have the BEST tour possible
			}

		}//for-k
	}//run

	int new_i = 0; // index into the new_pool

	void do_ga() {

		// * SELECT two individuals to mate
		Tour[] pair = selectTwo();

		// * CROSS-OVER
		// determine two cross-over points
		int x1, x2;
		x1 = Util.rndInt(1, numCities() - 2);
		x2 = Util.rndInt(x1, numCities() - 1);

		// determine the children by first crossing pair[0] with pair[1]
		// and then crossing pair[1] with pair[0]
		Tour t0, t1;
		t0 = pair[0].crossOver(pair[1], x1, x2);
		t1 = pair[1].crossOver(pair[0], x1, x2);

		// * MUTATE both c0 and c1
		t0.mutate();
		t1.mutate();

		// finally add c0 and c1 to the new pool
		new_pool[new_i] = t0;
		new_i++;
		new_pool[new_i] = t1;
		new_i++;
	}//do GA

	// randomly select two parents from the more fit population to mate
	Tour[] selectTwo() {
		Tour[] pair = new Tour[2];

		int k = POPL_SIZE / 2;

		pair[0] = old_pool[Util.rndInt(0, k)];
		pair[1] = old_pool[Util.rndInt(0, k)];

		return pair;
	}
	
	void showPool(Tour[] pool, String msg, int n) {
		System.out.println("\n"+msg);
		for (int i = 0; i < n; i++) {
			System.out.printf("  %3d %s\n", i, pool[i]);
		}
		double currTour = pool[0].totalDistance();
		String message = "Best current tour ";
		System.out.printf("%-18s%.2f%n", message, currTour);
	}

}
