// Raja Sooriamurthi
// S528 Object Oriented Programming

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.lang.Comparable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set; 
import java.util.Random;

public class Tour implements Comparable {

	static TSP tsp;

	int[] tourCities; // indices into the array of cities in TSP.java

	double distance;

	private Tour() {
		this.tourCities = new int[tsp.cities.length];
	}

	int length() {
		return tourCities.length;
	}

	// changed by Tyler Keyes
	public int compareTo(Object obj) {
		int compare;
		if(obj instanceof Tour) {
			Tour tour = (Tour) obj;
			double sum = (this.totalDistance() - tour.totalDistance());
			compare = (int) sum;
		} else {
			compare = 0;
		}
		
		return compare;
	}

	// the below constructor is used to make a clone
	Tour(Tour tr) {
		this();
		for (int i = 0; i < tr.length(); i++) {
			this.tourCities[i] = tr.tourCities[i];
		}
		this.distance = totalDistance();
	}

	static Tour sequence() {
		Tour tr = new Tour();
		for (int i = 0; i < tr.length(); i++) {
			tr.tourCities[i] = i;
		}
		tr.distance = tr.totalDistance();
		return tr;
	}

	static Tour rndTour() {
		Tour tr = Tour.sequence();
		tr.randomize();
		tr.distance = tr.totalDistance();
		return tr;
	}

	void randomize() {
		randomize(1, this.length());
	}

	void randomize(int i, int j) {
		for (int k = 0; k < (j - i + 1); k++) {
			rndSwapTwo(i, j);
		}
		this.distance = totalDistance();
	}

	void rndSwapTwo() {
		// index 0 has the start city and doesn't change
		// hence the random#s are from 1 onwards
		rndSwapTwo(1, this.length());
	}

	// randomly swap two cities between [i,j)
	void rndSwapTwo(int i, int j) {
		int a, b;
		int tmp;

		a = Util.rndInt(i, j-1);
		b = Util.rndInt(i, j-1);
		tmp = tourCities[a];
		tourCities[a] = tourCities[b];
		tourCities[b] = tmp;
	}

	City city(int i) {
		return tsp.cities[this.tourCities[i]];
	}

	// changed by Tyler Keyes
	// calculate the distance of the whole tour
	double totalDistance() {
		double dist = 0.0;
		City city1, city2;
		for (int i = 0, j = 1; i < this.length(); i++, j++) {
			int bound = this.length();
			if(j >= bound) // tests to see if j exceeds the length of the tour,
						   // then sets to 0 if true
				j = 0;
			city1 = city(i);
			city2 = city(j);
			dist += city1.distanceBetween(city2);
		}
		return dist;
		}

	public String toString() {
		String s = String.format("%6.1f :", this.distance);
		for (int i = 0; i < this.length(); i++) {
			s += String.format("%3d", tourCities[i]);
		}
		return s;
	}

	void mutate() {
		if (Math.random() < TSP.p_mutate) {
			// randomly flip two cities
			rndSwapTwo();
			this.distance = totalDistance();
		}
	}

	Tour crossOver(Tour p2, int x1, int x2) {
		/* Sets of p1 and p2 crossover Integer objects */
		Set<Integer> uniq = new HashSet<Integer>() ;
		Set<Integer> p1Cross = new HashSet<Integer>() ;
		Set<Integer> p2Cross = new HashSet<Integer>() ;

		//random used to determine direction of repair
		Random directionRand = new Random();
		
		// make an empty tour (child) and then fill it in below
		Tour child = new Tour() ;
		// copy the 1st segment of p1 (this) to the child
		for (int cityIndex = 0; cityIndex < x1 ; cityIndex++) {
			child.tourCities[cityIndex] = this.tourCities[cityIndex] ;
		
		}
	
		// copy the cross-over portion of p2 to the child
		for (int cityIndex = x1 ; cityIndex <= x2 ; cityIndex++) {
			child.tourCities[cityIndex] = p2.tourCities[cityIndex] ;
			
			/*Also add crossovers to sets*/
			p1Cross.add(new Integer(this.tourCities[cityIndex])) ;
			p2Cross.add(new Integer(p2.tourCities[cityIndex])) ;
			
		}
	
		
		// copy the last segment of p1 (this) to the child
		for  (int cityIndex = (x2 + 1); cityIndex < tourCities.length ; cityIndex++) {
			child.tourCities[cityIndex] = this.tourCities[cityIndex] ;
		}
		
		
		
		// Now we need to correct the child for any duplicate cities
		
		// First find out the unique elements of the cross-over segment
		// i.e., those elements of the cross-over segment of
		// p1 that are not in p2
		//COMP220 Requires use to use a Set to solve this problem
		
		for (Integer i : p1Cross) {
			if (!p2Cross.contains(i))
				uniq.add(i) ;
		}
		Iterator<Integer> uniqIter = uniq.iterator();
		// scan the two portions of p1 that have been crossed into the
		// the child for any duplicates in the crossed-over 
		// segment and if so replace with an element from the uniq list
		//Again, you are to use Java Sets here
		
		
		//if we 'lose' the coin flip we repair from beginning to end
		if(!directionRand.nextBoolean()) {
			
			//Scan first segment of child
			for (int cityIndex = 0; cityIndex < x1; cityIndex++) {
				if (p2Cross.contains(new Integer(child.tourCities[cityIndex]))) {
					
					int replace = (int)uniqIter.next() ;
					child.tourCities[cityIndex] = replace ;
				}
					
			}	
			
			//scan second segment
			for (int cityIndex = (x2 + 1); cityIndex < tourCities.length; cityIndex++) {
				if (p2Cross.contains(new Integer(child.tourCities[cityIndex]))) {
					
					int replace = (int)uniqIter.next() ;
					child.tourCities[cityIndex] = replace ;
				}
					
			}
		}
		
		//if we 'win' the coin toss we repair end to beginning
		else {
			
			//Scan second segment of child
			for (int cityIndex = tourCities.length - 1; cityIndex > x2; cityIndex--) {
				if (p2Cross.contains(new Integer(child.tourCities[cityIndex]))) {
					
					int replace = (int)uniqIter.next() ;
					child.tourCities[cityIndex] = replace ;
				}
					
			}	
			
			//scan first segment
			for (int cityIndex = (x1 - 1); cityIndex >= 0; cityIndex--) {
				if (p2Cross.contains(new Integer(child.tourCities[cityIndex]))) {
					
					int replace = (int)uniqIter.next() ;
					child.tourCities[cityIndex] = replace ;
				}
					
			}
		}
			
		child.distance = child.totalDistance();	//calculate fitness of this child
		return child;
	}
	

	void display(Map m) {
		m.tour = this;
		m.update(m.getGraphics());
	}

	void display(Graphics g) {
		final int SIZE = 2 * Map.CITY_SIZE;
		// plot the tour
		int len = this.length();
		int x1, y1, x2, y2;
		int i;
		for (i = 0; i < len - 1; i++) {
			x1 = city(i).x;
			y1 = city(i).y;
			x2 = city(i + 1).x;
			y2 = city(i + 1).y;
			g.setColor(Color.CYAN);
			g.drawLine(x1, y1, x2, y2);
			g.setColor(Color.RED);
			g.setFont(new Font("SansSerif", Font.BOLD, 16));
			g.drawString(Integer.toString(i), x1 - SIZE, y1 + SIZE);
		}
		// close the loop
		g.setColor(Color.CYAN);
		x1 = city(len - 1).x;
		y1 = city(len - 1).y;
		x2 = city(0).x;
		y2 = city(0).y;
		g.drawLine(x1, y1, x2, y2);
		g.setColor(Color.RED);
		g.drawString(Integer.toString(i), x1 - SIZE, y1 + SIZE);

	}
}
