import java.util.Random;

// Raja Sooriamurthi
// S528 Object Oriented Programming

public class Util {
	static Random myRand = new Random();
	
	// changed by Tyler Keyes
	static int rndInt(int lo, int hi) {
		//TO DO return r, such that lo <= r < hi
		int r = myRand.nextInt(hi - lo) + lo;
		return r;
	}

	// count the number of occurrence of key in a from start to end
	static int count(int[] a, int key, int start, int end) {
		int cnt = 0;	//return value
		int i;			//loop control
		
		for (i= start; i<=end; i++) {
			if (a[i] == key)
				cnt++;
		}
		return cnt;
	}

	static void pause(int n) {
		try {
			Thread.sleep(n);
		} catch (Exception e) {

		}
	}

}
