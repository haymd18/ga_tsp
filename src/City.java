// Raja Sooriamurthi
// S528 Object Oriented Programming


class City {

	int x, y;

	City(int x, int y) {
		this.x = x;
		this.y = y;
	}

	// made by Tyler Keyes
	double distanceBetween(City c) {
		int x1, x2, y1, y2;
		double dist, test;
		x1 = this.x; y1 = this.y;
		x2 = c.x; y2 = c.y;
		test = (Math.sqrt(Math.pow((x2-x1),2) + Math.pow((y2-y1),2)));
		dist = Math.abs(test);
		return dist;
	}

	public String toString() {
		return String.format("<City: %3d, %3d>", this.x, this.y);
	}
	


}