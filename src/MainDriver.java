
public class MainDriver {

	public static void main(String[] args) {

		int command = Integer.parseInt(args[0]) ;
		

		TSP tsp = new TSP(command);
		tsp.run();
		      
		SplitInterval si = new SplitInterval(15, 33, 1);
		int[] p = si.getIntervals();
		for (int i = 0; i < p.length; i++) {
			System.out.printf("%d : %d\n", i, p[i]);
		}
		
		City[] cities = GenerateCities.square(5);

		for (int i = 0; i < cities.length; i++) {
			System.out.printf("%3d -> %s\n", i, cities[i]);
		}
		
	}	//main

}	//MainDriver
